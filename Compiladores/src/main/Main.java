package main;
import java.io.PrintStream;
import java.util.LinkedList;
import interpret.Table;
import data.structures.*;
import interpret.Info;

public class Main {

	public static void main(String[] args) {
		PrintStream p = System.out;
		// a := 5 + 3 ; b := ( print ( a , a - 1 ) , 10 * a ) ; print ( b )
		Stm prog = new CompoundStm(new AssignStm("a",
				new OpExp(new NumExp(5),
						OpExp.Plus, new NumExp(3))),
				new CompoundStm(new AssignStm("b",
						new EseqExp(new PrintStm(new PairExpList(new IdExp("a"),
								new LastExpList(new OpExp(new IdExp("a"),
										OpExp.Minus, new NumExp(1))))),
				
								new OpExp(new NumExp(10), OpExp.Times,
										new IdExp("a")))),
						new PrintStm(new LastExpList(new IdExp("b")))));
		
		
		// Part 1
		LinkedList<Object> objects = new LinkedList<Object>();
		objects = prog.getResult();
		//p.println(objects);
		
		Info info = new Info();
		p.println(info.maxPrintArgs(objects));
		
		// Part 2
	    Table t = new Table("a",3,null);
	    t.update("a", 4);
		//p.print(t.lookup("a"));
	    
		t.interpStm(prog, t);
		// RASCUNHOS
		
		// Como reconhecer classes
				/*
				for (Object child : prog.getChilds()) {
					if (child instanceof AssignStm) {
						p.println("teste");
					}
				}
				*/
		
		//p.println(prog);
				//p.println(prog.getClass().getFields());
				
				/*for(Field f 
				 * ield : prog.getClass().getFields()) {
					//p.println(field.getDeclaringClass().getName());
					//p.println(field.getName());
					//field.setAccessible(true);
					if (field.getType().isAssignableFrom(AssignStm.class)) {
						p.print("cu");
					}
				}*/
	}
}
