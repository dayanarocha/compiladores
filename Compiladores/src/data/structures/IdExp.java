package data.structures;

import java.util.LinkedList;

public class IdExp extends Exp {
	public String id; 
	public IdExp(String i) {id=i;}
	@Override
	public Object[] getChilds() {
		// TODO Auto-generated method stub
		Object[] childs={id};
		return childs;
	}
	@Override
	public LinkedList<Object> getResult() {
		// TODO Auto-generated method stub
		LinkedList<Object> list = new LinkedList<Object>();
		list.add(this);
		list.add(id);
		return list;
	}
}
