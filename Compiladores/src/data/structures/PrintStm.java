package data.structures;

import java.util.LinkedList;

public class PrintStm extends Stm {
	 public ExpList exps;
	 public PrintStm(ExpList e) {
		 exps=e;
		 }
	@Override
	public Object[] getChilds() {
		// TODO Auto-generated method stub
		Object[] childs={exps};
		return childs;
	}
	@Override
	public LinkedList<Object> getResult() {
		// TODO Auto-generated method stub
		LinkedList<Object> list = new LinkedList<Object>();
		list.add(this);
		list.add(exps.getResult());
		return list;
	}
	 } 
