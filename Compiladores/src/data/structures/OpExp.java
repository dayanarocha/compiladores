package data.structures;

import java.util.LinkedList;

public class OpExp extends Exp {
	 public Exp left, right; public int oper;
	 final public static int Plus=1, Minus=2, Times=3, Div=4;
	 public OpExp(Exp l, int o, Exp r) {left=l; oper=o; right=r;}
	@Override
	public Object[] getChilds() {
		// TODO Auto-generated method stub
		Object[] childs={left,right};
		return childs;
	}
	@Override
	public LinkedList<Object> getResult() {
		// TODO Auto-generated method stub
		LinkedList<Object> list = new LinkedList<Object>();
		list.add(this);
		list.add(oper);
		list.add(left.getResult());
		list.add(right.getResult());
		return list;
	}}
	