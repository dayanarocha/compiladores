package data.structures;

import java.util.LinkedList;

public abstract class ExpList {
	public abstract Object[] getChilds();
	public abstract LinkedList<Object> getResult();
}
