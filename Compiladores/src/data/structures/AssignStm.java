package data.structures;

import java.util.LinkedList;

public class AssignStm extends Stm {
	 public String id; public Exp exp;
	 public AssignStm(String i, Exp e) {
		 id=i; 
		 exp=e;
		 }
	@Override
	public Object[] getChilds() {
		// TODO Auto-generated method stub
		Object[] childs={id,exp};
		return childs;
	}
	@Override
	public LinkedList<Object> getResult() {
		// TODO Auto-generated method stub
		LinkedList<Object> list = new LinkedList<Object>();
		list.add(this);
		list.add(id);
		list.add(exp.getResult());
		
		return list;
	}
	 
	 }
