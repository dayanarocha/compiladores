package data.structures;

import java.util.LinkedList;

public abstract class Stm {
	public abstract Object[] getChilds();
	public abstract LinkedList<Object> getResult();
	//public abstract Table interpStm(Stm s, Table t);
}
