package data.structures;

import java.util.LinkedList;

public abstract class Exp {
	public abstract Object[] getChilds();
	public abstract LinkedList<Object> getResult();
}
