package data.structures;

import java.util.LinkedList;

public class EseqExp extends Exp {
	 public Stm stm; public Exp exp;
	 public EseqExp(Stm s, Exp e) {stm=s; exp=e;}
	@Override
	public Object[] getChilds() {
		// TODO Auto-generated method stub
		Object[] childs={stm,exp};
		return childs;
	}
	@Override
	public LinkedList<Object> getResult() {
		// TODO Auto-generated method stub
		LinkedList<Object> list = new LinkedList<Object>();
		list.add(this);
		list.add(stm.getResult());
		list.add(exp.getResult());
		return list;
	}} 
