package data.structures;

import java.util.LinkedList;

public class NumExp extends Exp {
	 public int num;
	 public NumExp(int n) {num=n;}
	@Override
	public Object[] getChilds() {
		// TODO Auto-generated method stub
		Object[] childs={num};
		return childs;
	}
	@Override
	public LinkedList<Object> getResult() {
		// TODO Auto-generated method stub
		LinkedList<Object> list = new LinkedList<Object>();
		list.add(this);
		list.add(num);
		return list;
	}} 
