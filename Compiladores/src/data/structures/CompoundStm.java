package data.structures;

import java.util.LinkedList;

public class CompoundStm extends Stm {
	public Stm stm1, stm2;
	public CompoundStm(Stm s1, Stm s2) {
		stm1=s1;
		stm2=s2;
	}
	@Override
	public Object[] getChilds() {
		// TODO Auto-generated method stub
		Object[] childs={stm1,stm2};
		return childs;
	}
	@Override
	public LinkedList<Object> getResult() {
		// TODO Auto-generated method stub
		LinkedList<Object> list = new LinkedList<Object>();
		list.add(this);
		list.add(stm1.getResult());
		list.add(stm2.getResult());
		return list;
	}
	
}
