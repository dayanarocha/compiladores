package data.structures;

import java.util.LinkedList;

public class PairExpList extends ExpList {
	 public Exp head; 
	 public ExpList tail;
	 public PairExpList(Exp h, ExpList t) {head=h; tail=t;}
	@Override
	public Object[] getChilds() {
		// TODO Auto-generated method stub
		Object[] childs={head,tail};
		return childs;
	}
	@Override
	public LinkedList<Object> getResult() {
		// TODO Auto-generated method stub
		LinkedList<Object> list = new LinkedList<Object>();
		list.add(this);
		list.add(head.getResult());
		list.add(tail.getResult());
		return list;
	}} 