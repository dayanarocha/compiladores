package data.structures;

import java.util.LinkedList;

public class LastExpList extends ExpList {
	 public Exp head;
	 public LastExpList(Exp h) {head=h;}
	@Override
	public Object[] getChilds() {
		// TODO Auto-generated method stub
		Object[] childs={head};
		return childs;
	}
	@Override
	public LinkedList<Object> getResult() {
		// TODO Auto-generated method stub
		LinkedList<Object> list = new LinkedList<Object>();
		list.add(this);
		list.add(head.getResult());
		return list;
	}} 
