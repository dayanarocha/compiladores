package interpret;
import data.structures.EseqExp;
import data.structures.Exp;
import data.structures.ExpList;
import data.structures.IdExp;
import data.structures.LastExpList;
import data.structures.NumExp;
import data.structures.OpExp;
import data.structures.PairExpList;
public class IntAndTable {

    int i;
    Table t;

    IntAndTable(int ii, Table tt) {
        i = ii;
        t = tt;
    }

    IntAndTable interpExp(Exp e, Table t) {
    	if(e instanceof EseqExp) {
    		EseqExp eqe = (EseqExp) e;
        	t = t.interpStm(eqe.stm, t);
        	IntAndTable r = interpExp(eqe.exp, t);
        	this.i = r.i;
        	this.t = r.t;
        }
        else if(e instanceof IdExp) {
        	IdExp ie = (IdExp) e;
        	this.i = t.lookup(ie.id);
        }
        else if(e instanceof IdExp) {
        	IdExp ie = (IdExp) e;
        	this.i = t.lookup(ie.id);
        }
        else if(e instanceof NumExp) {
        	NumExp ne = (NumExp) e;
        	this.i = ne.num;
        }
        else if(e instanceof OpExp) {
        	OpExp oe = (OpExp) e;
        	if (oe.oper==1) {
        		this.i = interpExp(oe.left, t).i + interpExp(oe.right, t).i;
        	} else if (oe.oper==2) {
        		this.i = interpExp(oe.left, t).i - interpExp(oe.right, t).i;
        	} else if (oe.oper==3) {
        		this.i = interpExp(oe.left, t).i * interpExp(oe.right, t).i;
        	} else if (oe.oper==4) {
        		this.i = interpExp(oe.left, t).i / interpExp(oe.right, t).i;
        	}
        }
    	return this;
    }
    
    IntAndTable interpExpList(ExpList e, Table t) {
    	if(e instanceof LastExpList) {
    		LastExpList le = (LastExpList) e;
    		IntAndTable r = interpExp(le.head, t);
        	this.i = r.i;
        	this.t = r.t;
    	}else if(e instanceof PairExpList) {
    		PairExpList pl = (PairExpList) e;
    		IntAndTable r = interpExp(pl.head, t);
        	this.i = r.i;
        	this.t = r.t;
        	
        	r = interpExpList(pl.tail, t);
        	this.i = r.i;
        	this.t = r.t;
    	}
    	return this;
    }
    
    
    
    
}
