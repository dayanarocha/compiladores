package interpret;

import java.util.LinkedList;

import data.structures.ExpList;
import data.structures.PairExpList;
import data.structures.PrintStm;
import data.structures.Stm;

public class SearchList {
	
	LinkedList<Object> list;
	public SearchList(LinkedList<Object> list) {
		// TODO Auto-generated constructor stub
		this.list = list;
	}
		
	public int searchPrintStm(int num){
		for(Object obj:list) {
			if(obj instanceof LinkedList){
				SearchList s = new SearchList((LinkedList<Object>)obj);
				num = s.searchPrintStm(num);
			} 
			else if(obj instanceof PrintStm) {
				
				PrintStm ps = (PrintStm) obj;
				ExpList el = ps.exps;
				int params = 0;
				
				while(el instanceof PairExpList) {
					params += 1;
					el = ((PairExpList) el).tail;
				}
				// Caso em que temos a LastExpList
				params += 1;
				if(params > num) {
					num = params;
				}
			}
		}
		return num;	
	}
}
