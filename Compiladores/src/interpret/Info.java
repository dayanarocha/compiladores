package interpret;

import java.util.LinkedList;

public class Info {
	
	public int maxPrintArgs(LinkedList<Object> prog){
		SearchList s = new SearchList(prog);
		return s.searchPrintStm(0);
	}
}
