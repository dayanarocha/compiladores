package interpret;

import data.structures.AssignStm;
import data.structures.CompoundStm;
import data.structures.PairExpList;
import data.structures.PrintStm;
import data.structures.Stm;

public class Table {

    String id;
    int value;
    Table tail;
    IntAndTable iat;

    public Table(String i, int v, Table t) {
        id = i;
        value = v;
        tail = t;
        iat = new IntAndTable(0, t);
    }
    
    public int lookup(String key) {
    	
    	Integer result = Integer.parseInt("0");
    	result = null;
    	
    	if(id==key) {
    		result = value;
    	}else {
    		result = tail.lookup(key);
    	}
    	
    	return result;
    }
    
    public int update(String key, int value) {
    	if(value == lookup(key)) {
    		return -1;
    	}else if(id == key) {
    		this.value = value;
    		return 0;
    	}else if(tail != null) {
    		return tail.update(key, value);
    	}else {
    		return -1;
    	}
    }
    
    public Table interpStm(Stm s, Table t) {
        if(s instanceof AssignStm) {
        	AssignStm assig = (AssignStm) s;
        	t = new Table(assig.id, 
        			this.iat.interpExp(assig.exp, t).i,
        			t);
        }else if(s instanceof CompoundStm) {
        	CompoundStm comp = (CompoundStm) s;
        	t = interpStm(comp.stm1, t);
        	t = interpStm(comp.stm2, t);
        }else if(s instanceof PrintStm){
        	PrintStm pst = (PrintStm) s;
        	
        	if (pst.exps instanceof PairExpList) {
        		System.out.print(iat.interpExp(((PairExpList) pst.exps).head,t).i+ " ");
        		System.out.print(iat.interpExpList(((PairExpList) pst.exps).tail,t).i + " ");
        	}else {
        	System.out.println(iat.interpExpList(pst.exps, t).i);
        	}
        }
        return t;
    }

    /*
    int lookup(Table t, String key) {
        return 0;
    }
    
    int lookup(String key){
        return 0;
    }

    Table update(Table t1, String id, int value) {
        t1.id = id;
        t1.value = value;
        return t1;
    }
    */

}
